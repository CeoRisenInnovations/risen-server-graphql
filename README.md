#Risen Innovations GraphQL Server

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Node.js and MongoDB are required to run this server.

### Installing

A step by step series of examples that tell you how to get a development env running

In the directory that contains package.json install the required dependencies

```
yarn install
or
npm install
```

Insure that MongoDB is running with a matching Schema with:

```
src\graphql\schema.graphql
```

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

- [Express](https://expressjs.com/)
- [GraphQL](https://graphql.org/)
- [GraphQL-yoga](https://github.com/prisma/graphql-yoga)
- [NPM](https://www.npmjs.com/)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

- **Simon Masinde** - _Initial work_ - [Risen Innovations]()

## Acknowledgments

- Hat tip to all the devs that came before me.
