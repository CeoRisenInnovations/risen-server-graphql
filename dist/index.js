'use strict';

var _graphqlYoga = require('graphql-yoga');

require('regenerator-runtime/runtime');

var _db = require('./db');

var _resolvers = require('./graphql/resolvers');

var _resolvers2 = _interopRequireDefault(_resolvers);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _awsSdk = require('aws-sdk');

var _awsSdk2 = _interopRequireDefault(_awsSdk);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _cryptoJson = require('crypto-json');

var _cryptoJson2 = _interopRequireDefault(_cryptoJson);

require('dotenv/config');

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _expressRateLimit = require('express-rate-limit');

var _expressRateLimit2 = _interopRequireDefault(_expressRateLimit);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _process$env = process.env,
    MONGODB_DB_NAME = _process$env.MONGODB_DB_NAME,
    MONGODB_DB_URL = _process$env.MONGODB_DB_URL,
    MONGODB_DB_PORT = _process$env.MONGODB_DB_PORT,
    MONGODB_USER = _process$env.MONGODB_USER,
    MONGODB_PASSWORD = _process$env.MONGODB_PASSWORD;


var db = (0, _db.startDB)({
  user: MONGODB_USER,
  pwd: MONGODB_PASSWORD,
  db: MONGODB_DB_NAME,
  url: MONGODB_DB_URL + ':' + MONGODB_DB_PORT
});

_awsSdk2.default.config.region = 'us-east-1';

var transporter = _nodemailer2.default.createTransport({
  SES: new _awsSdk2.default.SES({
    apiVersion: '2010-12-01'
  })
});

var context = {
  models: _db.models,
  db: db
};

var server = new _graphqlYoga.GraphQLServer({
  typeDefs: __dirname + '/graphql/schema.graphql',
  resolvers: _resolvers2.default,
  context: context
});

server.express.use((0, _cors2.default)());

server.express.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

server.express.enable('trust proxy');

var limiter = (0, _expressRateLimit2.default)({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 25
});

server.express.use(limiter);

// parse application/x-www-form-urlencoded
server.express.use(_bodyParser2.default.urlencoded({ extended: true }));
// parse application/json
server.express.use(_bodyParser2.default.json());

// options
var opts = {
  port: process.env.PORT || 4000,
  endpoint: '/api',
  playground: '/api/playground'
};

server.express.post('/email', function (req, res) {
  console.log('EMAIL SENDING...', req.body);
  var algo = 'aes256';
  var encoding = 'base64';
  var keys = ['fullName', 'email', 'phoneNumber', 'company', 'location', 'projectIdea', 'projectType', 'website', 'estBudget'];
  var output = _cryptoJson2.default.decrypt(req.body, process.env.bcrypt, {
    encoding: encoding,
    keys: keys,
    algo: algo
  });
  res.send(output);

  transporter.sendMail({
    from: 'Risen Innovations <opportunities@riseninnovations.co.ke>',
    to: 'smasinde@gmail.com',
    subject: output.fullName + ' has sent a project request!',
    text: 'Estimated Budget: ' + output.estBudget + ' Company Name: ' + output.company + ' Email: ' + output.email + ' Phone Number: ' + output.phoneNumber + ' Location: ' + output.location + ' Website: ' + output.website + ' Project Type: ' + output.projectType + ' Project Idea: ' + output.projectIdea + ' SERVER: ' + process.env.APP_URL,
    ses: {}
  }, function (err, info) {
    console.log(info);
    console.log(err);
  });
});

var corsOptions = {
  origin: true,
  methods: ['GET', 'POST']
};

server.express.get('/', (0, _cors2.default)(corsOptions), function (req, res) {
  return res.send('Hello world!');
});

server.start(function (opts) {
  return console.log('Server is running on ' + process.env.APP_URL + ' and MongoDB on ' + MONGODB_DB_URL + ':' + MONGODB_DB_PORT);
});
//# sourceMappingURL=index.js.map