'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.models = exports.startDB = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _portfolio = require('./portfolio');

var _portfolio2 = _interopRequireDefault(_portfolio);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// SET UP Mongoose Promises.
_mongoose2.default.Promise = global.Promise;

var startDB = exports.startDB = function startDB(_ref) {
  var user = _ref.user,
      pwd = _ref.pwd,
      url = _ref.url,
      db = _ref.db;

  try {
    _mongoose2.default.connect('mongodb://' + user + ':' + pwd + '@' + url + '/' + db, { useNewUrlParser: true }).then(function () {
      console.log('MongoDB connection successful');
    }, function (err) {
      console.log(err);
    });
  } catch (err) {
    console.log('startDB.db error: ', err);
  }
};

var models = exports.models = {
  Portfolio: _portfolio2.default
};
//# sourceMappingURL=index.js.map