'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PortfolioSchema = new _mongoose2.default.Schema({
  name: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true
  },
  projectImage: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  shortName: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  }
});

exports.default = _mongoose2.default.model('Portfolio', PortfolioSchema);
//# sourceMappingURL=portfolio.js.map