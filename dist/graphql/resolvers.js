"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

exports.default = {
  Query: {
    portfolio: function () {
      var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(parent, args, _ref2) {
        var models = _ref2.models;
        var Portfolio;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return models.Portfolio.find({});

              case 2:
                Portfolio = _context.sent;
                return _context.abrupt("return", Portfolio);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, undefined);
      }));

      return function portfolio(_x, _x2, _x3) {
        return _ref.apply(this, arguments);
      };
    }()
  }
};
//# sourceMappingURL=resolvers.js.map