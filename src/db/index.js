import mongoose from 'mongoose';
import Portfolio from './portfolio';

// SET UP Mongoose Promises.
mongoose.Promise = global.Promise;

export const startDB = ({ user, pwd, url, db }) => {
  try {
    mongoose
      .connect(
        `mongodb://${user}:${pwd}@${url}/${db}`,
        { useNewUrlParser: true }
      )
      .then(
        () => {
          console.log('MongoDB connection successful');
        },
        err => {
          console.log(err);
        }
      );
  } catch (err) {
    console.log('startDB.db error: ', err);
  }
};

export const models = {
  Portfolio
};
