import mongoose from 'mongoose';

const PortfolioSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  location: {
    type: String,
    required: true
  },
  projectImage: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  shortName: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  }
});

export default mongoose.model('Portfolio', PortfolioSchema);
