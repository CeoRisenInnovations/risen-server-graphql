export default {
  Query: {
    portfolio: async (parent, args, { models }) => {
      const Portfolio = await models.Portfolio.find({});
      return Portfolio;
    }
  }
};
