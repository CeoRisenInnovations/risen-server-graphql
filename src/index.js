import { GraphQLServer } from 'graphql-yoga';
import 'regenerator-runtime/runtime';
import { startDB, models } from './db';
import resolvers from './graphql/resolvers';
import nodemailer from 'nodemailer';
import aws from 'aws-sdk';
import bodyParser from 'body-parser';
import cryptoJSON from 'crypto-json';
import 'dotenv/config';
import cors from 'cors';
import rateLimit from 'express-rate-limit';

const {
  MONGODB_DB_NAME,
  MONGODB_DB_URL,
  MONGODB_DB_PORT,
  MONGODB_USER,
  MONGODB_PASSWORD
} = process.env;

const db = startDB({
  user: MONGODB_USER,
  pwd: MONGODB_PASSWORD,
  db: MONGODB_DB_NAME,
  url: `${MONGODB_DB_URL}:${MONGODB_DB_PORT}`
});

aws.config.region = 'us-east-1';

let transporter = nodemailer.createTransport({
  SES: new aws.SES({
    apiVersion: '2010-12-01'
  })
});

const context = {
  models,
  db
};

const server = new GraphQLServer({
  typeDefs: `${__dirname}/graphql/schema.graphql`,
  resolvers,
  context
});

server.express.use(cors());

server.express.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

server.express.enable('trust proxy');

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 25
});

server.express.use(limiter);

// parse application/x-www-form-urlencoded
server.express.use(bodyParser.urlencoded({ extended: true }));
// parse application/json
server.express.use(bodyParser.json());

// options
const opts = {
  port: process.env.PORT || 4000,
  endpoint: '/api',
  playground: '/api/playground'
};

server.express.post('/email', (req, res) => {
  console.log('EMAIL SENDING...', req.body);
  const algo = 'aes256';
  const encoding = 'base64';
  const keys = [
    'fullName',
    'email',
    'phoneNumber',
    'company',
    'location',
    'projectIdea',
    'projectType',
    'website',
    'estBudget'
  ];
  const output = cryptoJSON.decrypt(req.body, process.env.bcrypt, {
    encoding,
    keys,
    algo
  });
  res.send(output);

  transporter.sendMail(
    {
      from: 'Risen Innovations <opportunities@riseninnovations.co.ke>',
      to: 'smasinde@gmail.com',
      subject: `${output.fullName} has sent a project request!`,
      text: `Estimated Budget: ${output.estBudget} Company Name: ${
        output.company
      } Email: ${output.email} Phone Number: ${output.phoneNumber} Location: ${
        output.location
      } Website: ${output.website} Project Type: ${
        output.projectType
      } Project Idea: ${output.projectIdea} SERVER: ${process.env.APP_URL}`,
      ses: {}
    },
    (err, info) => {
      console.log(info);
      console.log(err);
    }
  );
});

var corsOptions = {
  origin: true,
  methods: ['GET', 'POST']
};

server.express.get('/', cors(corsOptions), (req, res) =>
  res.send('Hello world!')
);

server.start(opts =>
  console.log(
    `Server is running on ${
      process.env.APP_URL
    } and MongoDB on ${MONGODB_DB_URL}:${MONGODB_DB_PORT}`
  )
);
